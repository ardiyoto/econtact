import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController, LoadingController, NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {
  
  contact : any = {}

  constructor(
    public http:HttpClient, 
    public loading:LoadingController,
    public navCtrl:NavController,
    public router:ActivatedRoute,
    public toast:ToastController,
    public ac:ActionSheetController
  ) { }

  ngOnInit() {
    this.router.queryParams.subscribe(param =>{
      this.contact.id = param.id
      this.contact.nama = param.nama
      this.contact.phone = param.phone
      this.contact.email = param.email
      this.contact.deskripsi = param.deskripsi
      this.contact.foto = param.foto
      console.log(param)
    })
  }

  async saveData(){

    /* Validation */
    if(!this.contact.nama){
      this.notif("Nama tidak boleh kosong !")
      return
    }
    if(!this.contact.phone){
      this.notif("No Telepon tidak boleh kosong !")
      return
    }

    if(this.contact.email && !this.validateEmail(this.contact.email)){
      this.notif("Maaf email tidak valid")
      return
    }

    /* Loading */
    const loading = await this.loading.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present()

    /* Send Data to Save */
    let headers : any	= new HttpHeaders()
    headers.append('Accept', 'application/json')
    headers.append('Content-Type', 'application/json')
  
    let url = this.contact.id ? "https://ik12021.000webhostapp.com/update.php" : "https://ik12021.000webhostapp.com/save.php"

    await this.http.post(url,JSON.stringify(this.contact),headers).toPromise()
    .then(res=>{
      console.log(res)
      loading.dismiss()
      this.navCtrl.back()
    })

    console.log(this.contact)
  }

  validateEmail(email:any) {
    const format = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return format.test(email);
  } 

  async notif(text:any,duration:any=2000){
    const toast = await this.toast.create({
      message: text,
      duration: duration,
      color: "danger"
    });
    toast.present();
  }

  setImage(event:any){
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      // Mengganti src dari object img#avatar
      reader.onload = (e:any) => {
        console.log(e.target.result)
        this.contact.foto = e.target.result // Set ke variable foto
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  removeImage(){
    this.contact.foto = ""
  }

  async showMenu(obj :any){
    const actionSheet = await this.ac.create({
      header: 'Picture',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Ambil Gambar',
          icon: 'images',
          handler: () => {
            obj.click()
          }
        }, {
          text: 'Hapus Gambar',
          icon: 'trash',
          handler: () => {
            this.removeImage()
          }
        }
      ]
    });
    await actionSheet.present();
  }

}


