import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  searchBar : any = false
  contacts : any
  keyword : any

  constructor(
    public http:HttpClient
  ) {
    //this.getData()
  }

  ionViewDidEnter(){
    this.getData()
  }

  viewSearchBar(){
    this.searchBar = !this.searchBar
    this.keyword = ""
  }

  async getData(){
    await this.http.get("https://ik12021.000webhostapp.com/data.php").toPromise()
    .then(res => {
      this.contacts = res   
      console.log(this.contacts)   
    })    
  }

  async deleteData(id:any){
    console.log(id)
    await this.http.get("https://ik12021.000webhostapp.com/delete.php?id="+id).toPromise()
    .then(res => {
      this.getData();
    })
  }

  async favorite(id:any,status:any){
    let fav = status == 0 ? 1 : 0
    await this.http.get("https://ik12021.000webhostapp.com/favorit.php?id="+id+"&fav="+fav).toPromise()
    .then(res => {
      this.getData();
    })
  }

}
