import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  contacts : any

  constructor(
    public http: HttpClient
  ) {
   
  }

  ionViewDidEnter(){
    this.getData()
  }

  async getData(){
    await this.http.get("https://ik12021.000webhostapp.com/data.php?fav=1").toPromise()
    .then(res => {
      this.contacts = res   
      console.log(this.contacts)   
    }) 
  }

}
